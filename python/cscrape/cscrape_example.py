# Cookie Scrape
# This is based on the example in https://github.com/borisbabic/browser_cookie3, but in Python 3

import re
import urllib
import urllib.request
import browser_cookie3
url = "https://codeberg.org/"
public_html = urllib.request.urlopen(url).read()
get_title = lambda html: re.findall("<title>(.*?)</title>", html, flags = re.DOTALL)[0].strip()
get_title(str(public_html))
cj = browser_cookie3.firefox() # Change to whatever browser you're using
opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
login_html = opener.open(url).read()
get_title(str(login_html))

# Python 3.9.7 (default, Aug 31 2021, 13:28:12) 
# [GCC 11.1.0] on linux
# Type "help", "copyright", "credits" or "license" for more information.
# >>> import re
# >>> import urllib
# >>> import urllib.request
# >>> import browser_cookie3
# >>> url = "https://codeberg.org/"
# >>> public_html = urllib.request.urlopen(url).read()
# >>> get_title = lambda html: re.findall("<title>(.*?)</title>", html, flags = re.DOTALL)[0].strip()
# >>> get_title(str(public_html))
# 'Codeberg.org'
# >>> cj = browser_cookie3.firefox()
# >>> opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
# >>> login_html = opener.open(url).read()
# >>> get_title(str(login_html))
# 'unter - Dashboard -  Codeberg.org'