#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '
# PS1='[\u@\h \w]\$ ' # New one, with directory shown

# PS1="%F{%(#.blue.green)}┌──${arch-chroot:+($arch-chroot)─}${VIRTUAL_ENV:+($(basename $VIRTUAL_ENV))─}(%B%F{%(#.red.blue)}%n$prompt_symbol%m%b%F{%(#.blue.green)})-[%B%F{reset}%(6~.%-1~/…/%4~.%5~)%b%F{%(#.blue.green)}]└─%B%(#.%F{red}#.%F{blue}$)%b%F{reset}"

RED="\[$(tput setaf 1)\]"
GREEN="\[$(tput setaf 2)\]"
BLUE="\[$(tput setaf 4)\]"
RESET="\[$(tput sgr0)\]"

PS1="${GREEN}┌──(${BLUE}\u@\h${GREEN})-[${RESET}\w${GREEN}]\n${GREEN}└─${BLUE}$ ${RESET}"
PS2="%_>"
PS3="?#"
