#!/bin/bash
# Install yay on Arch

echo "Current username: "
read username

sudo pacman -S base-devel git
cd /opt
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R $username:users ./yay

cd yay
makepkg -si